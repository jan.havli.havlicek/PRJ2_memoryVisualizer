package Model;

import java.util.ArrayList;

public class Method
{
    private String name;
    private String file;
    private int lineNumber;
    private ArrayList<String> lines;
    private ArrayList<HeapObject> heap;
    private ArrayList<StackObject> stack;
    private Method previous;
    private Method next;

    public Method()
    {
        this.name = "defaultName";
        this.file = "defaultFile.java";
        this.lineNumber = 0;
    }

    public Method(String name, String file, int lineNumber, ArrayList<String> lines)
    {
        this.name = name;
        this.file = file;
        this.lineNumber = lineNumber;
        this.lines = lines;
    }

    public String getName()
    {
        return this.name;
    }

    public String getFile()
    {
        return this.file;
    }

    public void setPrevious(Method prev)
    {
        this.previous = prev;
    }

    public void setNext(Method next)
    {
        this.next = next;
    }

    public void setMethodLines(ArrayList<String> lines)
    {
        this.lines = lines;
    }

    /**
     *
     * ToDo - check if all the primitive types are the only ones that goes to stack (+ methods...)
     * */
    public void readObjects()
    {
        for(String line : this.lines)
        {
            if(line.contains("new"))
            {
                this.heap.add(new HeapObject(line));
            }
            else if(line.contains("int") || line.contains("char") || line.contains("byte")
                    || line.contains("short") || line.contains("long") || line.contains("float")
                    || line.contains("double") || line.contains("boolean"))  //ToDo - better use dictionary
            {
                this.stack.add(new StackObject(line));
            }
        }
    }

}
