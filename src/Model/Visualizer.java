package Model;

import com.sun.jdi.Bootstrap;
import com.sun.jdi.ThreadReference;
import com.sun.jdi.VirtualMachine;
import com.sun.jdi.VirtualMachineManager;
import com.sun.jdi.connect.AttachingConnector;
import com.sun.jdi.connect.Connector;
import com.sun.jdi.connect.IllegalConnectorArgumentsException;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

public class Visualizer
{
    private ArrayList<Method> methodTrace;

    public Visualizer()
    {
        this.methodTrace = new ArrayList<Method>();
    }

    public void visualize()
    {
        /**
         * https://stackoverflow.com/questions/20441256/contents-of-stack-frame-in-java
         * https://alvinalexander.com/scala/fp-book/recursion-jvm-stacks-stack-frames
         * https://docs.oracle.com/javase/8/docs/jdk/api/jpda/jdi/com/sun/jdi/ThreadReference.html#frames--
         * https://www.programcreek.com/java-api-examples/?class=com.sun.jdi.ThreadReference&method=frame
         * https://docs.oracle.com/javase/7/docs/jdk/api/attach/spec/com/sun/tools/attach/VirtualMachine.html
         * https://www.programcreek.com/java-api-examples/?class=com.sun.jdi.ThreadReference&method=frames
         *
         * !! https://stackoverflow.com/questions/42888218/jdi-threadreference-frame-causes-incompatiblethreadstateexception
         * ! JDI !
         * examples: https://docs.oracle.com/javase/7/docs/technotes/guides/jpda/examples.html
         * tutorial: https://www.tutorialspoint.com/jdb/jdb_introduction.htm
         *
         * https://stackoverflow.com/questions/47939691/build-a-simple-debugger-with-jdi-to-set-breakpoints-and-retrieve-the-value-of-a
         *
         * Looking for some kind of result here with StackFrame. Lot of digging on how to properly use the StackFrame (bad orientation in doc)
         * Now it should be running (thus I do not understand the AttachingConnector creation), but it goes with java.io.IOException: Can not attach to current VM
         *
         * 181106 - trying to handle (get rid of) the exception
         *              - Be careful! https://docs.oracle.com/javase/7/docs/jdk/api/jpda/jdi/com/sun/jdi/VirtualMachineManager.html
         *              - tested memorySharedAttachingConnector - not working either (plus I think that it is not the one I need)
         *              - https://github.com/raphw/byte-buddy/issues/295
         *                  - http://jigsaw-dev.1059479.n5.nabble.com/Disallowing-the-dynamic-loading-of-agents-by-default-revised-td5716181i20.html
         *
         * */
        //tests...

        //https://www.programcreek.com/java-api-examples/index.php?api=com.sun.jdi.connect.AttachingConnector
        VirtualMachineManager vmManager = Bootstrap.virtualMachineManager();
        AttachingConnector processAtt = vmManager.attachingConnectors()
                .stream()
                .filter(connector -> connector.name().equals("com.sun.jdi.ProcessAttach"))
                .findFirst()
                .orElse(null);

        if(processAtt == null)
        {
            return;
        }

        try
        {
            Map<String, Connector.Argument> args = processAtt.defaultArguments();
            Connector.StringArgument arg = (Connector.StringArgument) args.get("pid");
            arg.setValue(ProcessHandle.current().pid() + "");
            Connector.IntegerArgument arg2 = (Connector.IntegerArgument) args.get("timeout");
            arg2.setValue(3000);

            System.out.println(processAtt.description());
            VirtualMachine vm = processAtt.attach(args);
            //ThreadReference ref = vm.allThreads();
            System.out.println("ATTACHED!");
        } catch (IOException e)
        {
            //Cannot attach to current VM exception
            System.out.println("Exception" + e.toString());
//          e.printStackTrace()
        } catch (IllegalConnectorArgumentsException e)
        {
            e.printStackTrace();
        }

/*
        try
        {
            Map<String, Connector.Argument> argsMemSh = memorySharedAtt.defaultArguments();
            Connector.StringArgument argsh1 = (Connector.StringArgument) argsMemSh.get("name");
            //argsh1.setValue(ProcessHandle.current() + "");
            VirtualMachine testing = memorySharedAtt.attach(argsMemSh);
        } catch (IOException e)
        {
            //Cannot attach to current VM exception
            e.printStackTrace();
        } catch (IllegalConnectorArgumentsException e)
        {
            e.printStackTrace();
        }
*/
        /*fillMethodTrace();

        for(Method met : this.methodTrace)
        {

        }*/
    }

    private void fillMethodTrace()
    {
        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        for(StackTraceElement el : elements)
        {
            String path = getPathToFile(el.getClassName().substring(el.getClassName().indexOf('.')+1) + ".java");
            ArrayList<String> lines = readMethod(path, el.getMethodName());
            if(lines.size() > 0)
            {
                Method loc = new Method(el.getMethodName(), path, el.getLineNumber(), lines);
                if(!loc.getName().contains("visualize")){
                    this.methodTrace.add(loc);

                    //ToDo - not try-catch, better to use collection.size() condition!
                    try
                    {
                        this.methodTrace.get(this.methodTrace.size() - 1).setNext(this.methodTrace.get(this.methodTrace.size() - 2));
                    }catch(Exception ex){}
                    try
                    {
                        this.methodTrace.get(this.methodTrace.size() - 2).setPrevious(this.methodTrace.get(this.methodTrace.size() - 1));
                    }catch(Exception ex){}
                }
            }
        }
    }

    private String getPathToFile(String fileName)
    {
        String path = Paths.get(".").toAbsolutePath().normalize().toString();
        String res = "";

        try
        {
            //FileUtils - knihovna "3ti" strany
            Collection files = FileUtils.listFiles(new File(path), null, true);

            for(Iterator it = files.iterator(); it.hasNext();)
            {
                File f = (File) it.next();
                if(f.getName().contains(fileName))
                {
                    res = f.getAbsolutePath();
                    break;
                }
            }
        }catch(Exception ex)
        {

        }

        return res;
    }

    private ArrayList<String> readMethod(String file, String methodName)
    {
        ArrayList<String> methodLines = new ArrayList<String>();
        int leftBracketCounter = 0, rightBracketCounter = 0;

        try{
            File toRead = new File(file);
            Scanner scanner = new Scanner(toRead);

            while(scanner.hasNext())
            {
                String line = scanner.nextLine();

                if(methodName.contains("init"))
                    methodName = file.substring(file.lastIndexOf('\\')+1, file.lastIndexOf('.'));

                if(isMethodSignature(line, methodName))
                {
                    do{
                        methodLines.add(line);
                        line = scanner.nextLine();
                        if(line.contains("{"))  leftBracketCounter++;
                        if(line.contains("}")) rightBracketCounter++;
                        if(line.contains("}") && rightBracketCounter == leftBracketCounter) break;
                    }while(true);
                    methodLines.add(line);
                    break;
                }
            }
        }catch(FileNotFoundException fnf)
        {

        }

        return methodLines;
    }

    private boolean isMethodSignature(String line, String name)
    {
        if((line.contains("private") || line.contains("public") || line.contains("protected"))
                && line.contains(name) && !line.contains("class"))
            return true;
        else
            return false;
    }
}
