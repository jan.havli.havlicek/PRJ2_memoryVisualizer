package Model;

public class SampleClass
{
    private Visualizer visualizer;

    public SampleClass()
    {
        this.visualizer = new Visualizer();
        test();
    }

    public void test()
    {
        add(5, 3);
    }

    public void add(int a, int b)
    {
        int c = a + b;

        nextOne(c);
    }

    public void nextOne(int c)
    {
        System.out.println(c + "\n");

        this.visualizer.visualize();
    }
}
