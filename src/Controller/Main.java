package Controller;

//https://www.journaldev.com/4098/java-heap-space-vs-stack-memory
//??? https://blogs.oracle.com/sundararajan/programmatically-dumping-heap-from-java-applications
//??? http://vlkan.com/blog/post/2016/08/12/hotspot-heapdump-threadump/

//https://medium.com/fhinkel/confused-about-stack-and-heap-2cf3e6adb771
//Value types are stored on stack, reference types in heap...       //modifier NEW goes to HEAP
//How about to recursively go through code using the StackTrace

//Nechci stackTrace, nýbrž vizualizaci uložených hodnot ve STACK a HEAP

/**
 * @author Havlicek Jan, A17B0031K
 *
 * This application should serve as memory visualizer for
 * teaching purposes. It will visualize HEAP and STACK memory spaces status
 * with all the objets stored there.
 *
 * Basically, there is not a native class that can list all the objects from heap, nor from stack,
 * with its informations, so the application will be concipied as interface. When called (the right way),
 * it will find its location in code, goes through all methods in StackTrace, then read its code from file
 * and then read all the objects and visualize them on canvas.
 *
 * In future it may be possible to visualize the memory allocation simultaneously and continually when
 * the application is running.
 */
public class Main {
    public static void main(String[] args)
    {
        new ControllerMain();
    }
}
